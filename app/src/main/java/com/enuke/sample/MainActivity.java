package com.enuke.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN";
    DatabaseReference myRef;
    ValueEventListener valueEventListener;
    EditText name,email;
    Button save,retrieve;
    TextView datatv;

    int count = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        save = findViewById(R.id.save);
        retrieve = findViewById(R.id.retrieve);
        datatv = findViewById(R.id.dataTv);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(name.getText()) && !TextUtils.isEmpty(email.getText())) {
                    writeNewUser(count + "", name.getText().toString(), email.getText().toString());
                    name.setText("");
                    email.setText("");
                    datatv.setText("");
                    count++;
                }
            }
        });

        retrieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromFirebase();
            }
        });

        // Write a message to the database
        //myRef = database.getReference("message");
        // User user = new User("Amrit", "Saini");
        //myRef.setValue(user);



    }

    private void writeNewUser(String userId, String name, String email) {
        User user = new User(name, email);

        myRef.child("users").child(userId).setValue(user);
    }

    private void getDataFromFirebase(){
        // Read from the database
       /* myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                User value = dataSnapshot.getValue(User.class);
                Log.d(TAG, "Value is: " + value.getFirstName());
                //Log.e(TAG,new Gson().toJson(dataSnapshot));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    String s = "";
                    for(int i=1;i<=dataSnapshot.child("users").getChildrenCount();i++) {
                        User user = dataSnapshot.child("users").child(i + "").getValue(User.class);
                        //User user = dataSnapshot.getValue(User.class);
                        s += user.getName() + " " + user.getEmail() + "\n";
                    }
                    datatv.setText(s);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        myRef.addValueEventListener(valueEventListener);
        this.valueEventListener = valueEventListener;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(valueEventListener != null) {
            myRef.removeEventListener(valueEventListener);
        }
    }
}
