package com.enuke.sample;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.POST;


/**
 * Created by enuke on 12/8/17.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    Context context;
    ArrayList<Post> posts;
    ArrayList<String> postKeys;

    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;

    public PostAdapter(Context context, final ArrayList<Post> posts, DatabaseReference ref) {
        this.context = context;
        this.posts = posts;
        this.mDatabaseReference = ref;
        setPostKeys();

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("onChildAdded:", dataSnapshot.getKey());

                // A new post has been added, add it to the displayed list
                Post post = dataSnapshot.getValue(Post.class);

                // [START_EXCLUDE]
                // Update RecyclerView
                posts.add(post);
                notifyItemInserted(getItemCount() - 1);
                // [END_EXCLUDE]
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("onChildChanged:",  dataSnapshot.getKey());

                // A post has changed, use the key to determine if we are displaying this
                // post and if so displayed the changed post.
                Post newPost = dataSnapshot.getValue(Post.class);
                String postKey = dataSnapshot.getKey();

                // [START_EXCLUDE]
                int postIndex = postKeys.indexOf(postKey);
                if (postIndex > -1) {
                    // Replace with the new data
                    posts.set(postIndex, newPost);

                    // Update the RecyclerView
                    notifyItemChanged(postIndex);
                } else {
                    Log.w("onChildChanged:unknown_child:", postKey);
                }
                // [END_EXCLUDE]
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("onChildRemoved:", dataSnapshot.getKey());

                // A post has changed, use the key to determine if we are displaying this
                // post and if so remove it.
                String postKey = dataSnapshot.getKey();

                // [START_EXCLUDE]
                int postIndex = postKeys.indexOf(postKey);
                if (postIndex > -1) {
                    // Remove data from the list
                    postKeys.remove(postIndex);
                    posts.remove(postIndex);

                    // Update the RecyclerView
                    notifyItemRemoved(postIndex);
                } else {
                    Log.w("onChildRemoved:unknown_child:", postKey);
                }
                // [END_EXCLUDE]
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d( "onChildMoved:", dataSnapshot.getKey());

                // A post has changed position, use the key to determine if we are
                // displaying this post and if so move it.
                Post movedPost = dataSnapshot.getValue(Post.class);
                String postKey = dataSnapshot.getKey();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("postComments:onCancelled", databaseError.toException());
                Toast.makeText(context, "Failed to load comments.",
                        Toast.LENGTH_SHORT).show();
            }
        };

        ref.addChildEventListener(childEventListener);
        // [END child_event_listener_recycler]

        // Store reference to listener so it can be removed on app stop
        mChildEventListener = childEventListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item,null,false);
        ViewHolder holder=new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = posts.get(position);
        holder.authorText.setText(post.getAuthor());
        holder.titleText.setText(post.getTitle());
        holder.bodyText.setText(post.getBody());
        holder.startText.setText(post.getStarCount()+"");
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView authorText,titleText,bodyText,startText;
        public ViewHolder(View itemView) {
            super(itemView);
            authorText = itemView.findViewById(R.id.author);
            titleText = itemView.findViewById(R.id.title);
            bodyText = itemView.findViewById(R.id.body);
            startText = itemView.findViewById(R.id.stars);
        }
    }

    private void setPostKeys(){
        postKeys = new ArrayList<>();
        for(Post post:posts){
            postKeys.add(post.getPostId());
        }
    }

    public void cleanupListener() {
        if (mChildEventListener != null) {
            mDatabaseReference.removeEventListener(mChildEventListener);
        }
    }
}
