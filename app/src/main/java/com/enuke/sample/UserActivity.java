package com.enuke.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserActivity extends AppCompatActivity {

    RecyclerView recycler;
    UserAdapter adapter;
    ArrayList<User> users;
    User senderUser;

    DatabaseReference databaseReference;
    DatabaseReference usersReference;
    ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        users = new ArrayList<>();
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference();
        usersReference = db.getReference().child("users");

        recycler = findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        recycler.setLayoutManager(layoutManager);

        adapter = new UserAdapter(this, users,databaseReference);
        recycler.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(users.size() == 0) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        String id = data.getKey();
                        User user = data.getValue(User.class);
                        user.setId(id);
                        users.add(user);
                        adapter.notifyItemInserted(0);
                        adapter.notifyDataSetChanged();
                    }

                    senderUser = users.get(0);
                    adapter.setSender(senderUser);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        usersReference.addValueEventListener(valueEventListener);

        this.valueEventListener = valueEventListener;
    }
}
