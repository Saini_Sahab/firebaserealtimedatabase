package com.enuke.sample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by enuke on 12/8/17.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    Context context;
    ArrayList<User> users;
    DatabaseReference databaseReference;
    User senderUser;

    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;

    public UserAdapter(Context context, final ArrayList<User> users, DatabaseReference databaseReference) {
        this.context = context;
        this.users = users;
        this.databaseReference = databaseReference;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, null, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);
        holder.idText.setText(user.getId());
        holder.nameText.setText(user.getName());

        holder.idText.setOnClickListener(view -> {
            openChat(user);
        });

        holder.nameText.setOnClickListener(view -> {
            openChat(user);
        });
    }

    private void openChat(User recieverUser) {
        //String key = databaseReference.child("users").child(senderUser.getId()).push().getKey();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/friends/" + senderUser.getId(), recieverUser.getId());
        childUpdates.put("/friends/" + recieverUser.getId(), senderUser.getId());
        //childUpdates.put("/user-posts/" + post.getUid() + "/" + key, postValues);

        databaseReference.updateChildren(childUpdates);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView idText, nameText;

        public ViewHolder(View itemView) {
            super(itemView);
            idText = itemView.findViewById(R.id.id);
            nameText = itemView.findViewById(R.id.name);
        }
    }

    public void setSender(User senderUser) {
        this.senderUser = senderUser;
    }

}
