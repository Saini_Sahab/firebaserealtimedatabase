package com.enuke.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;

public class PostActivity extends AppCompatActivity implements View.OnClickListener {

    DatabaseReference databaseReference;
    DatabaseReference postChildReference;
    ValueEventListener valueEventListener;
    ChildEventListener childEventListener;

    PostAdapter adapter;
    EditText titleText, bodyText, idText, authorText;
    Button submit;
    RecyclerView recycler;
    ArrayList<Post> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference();
        postChildReference = FirebaseDatabase.getInstance().getReference().child("posts");

        titleText = findViewById(R.id.title);
        bodyText = findViewById(R.id.body);
        idText = findViewById(R.id.id);
        authorText = findViewById(R.id.author);
        submit = findViewById(R.id.submit);
        recycler = findViewById(R.id.recycler);

        submit.setOnClickListener(this);
        posts = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);
        recycler.setLayoutManager(layoutManager);

        adapter = new PostAdapter(this, posts, postChildReference);
        recycler.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("posts")) {
                    boolean listUpdated = false;
                    for (DataSnapshot postData : dataSnapshot.child("posts").getChildren()) {
                        if (posts.size() == 0) {
                            listUpdated = true;
                            Post post = postData.getValue(Post.class);
                            posts.add(post);
                        }
                    }

                    if(listUpdated){
                        //Collections.reverse(posts);
                        if (adapter != null) {
                            adapter.notifyItemInserted(0);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference.addValueEventListener(valueEventListener);

        this.valueEventListener = valueEventListener;

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }

        if (adapter != null) {
            adapter.cleanupListener();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (!TextUtils.isEmpty(authorText.getText())
                        && !TextUtils.isEmpty(idText.getText())
                        && !TextUtils.isEmpty(titleText.getText())
                        && !TextUtils.isEmpty(bodyText.getText())) {
                    Post post = new Post();
                    post.setUid(idText.getText().toString());
                    post.setAuthor(authorText.getText().toString());
                    post.setTitle(titleText.getText().toString());
                    post.setBody(bodyText.getText().toString());
                    post.setStarCount(5);
                    writeNewPost(post);
                    Toast.makeText(this, "Posted", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void writeNewPost(Post post) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = databaseReference.child("posts").push().getKey();
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + post.getUid() + "/" + key, postValues);

        databaseReference.updateChildren(childUpdates);
    }
}
