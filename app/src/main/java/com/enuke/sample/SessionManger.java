package com.enuke.sample;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManger {

    private static String PREF_NAME="TrackingSession";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    Context context;

    private String isLogin="isLoggedIn";

     public SessionManger(Context context){
         preferences=context.getSharedPreferences(PREF_NAME,0);
         editor=preferences.edit();
         this.context=context;
     }

    public void setIsLoggedIn(boolean value){
        editor.putBoolean(isLogin,value);
        editor.commit();
    }
    public boolean isLoggedIn(){
        return preferences.getBoolean(isLogin,false);
    }

    public void logoutSession(){
        editor.clear();
        editor.commit();
    }

    public void setUserId(String userId){
        editor.putString("userId",userId);
        editor.commit();
    }

    public String getUserId(){
       return preferences.getString("userId","");
    }

    public void setCurrentTripId(String id){
        editor.putString("currentTrip",id);
        editor.commit();
    }

    public String getCurrentTripId(){
      return   preferences.getString("currentTrip","");
    }

    public void saveTripTIme(float value){
        editor.putFloat("triptime",value);
        editor.commit();
    }

    public void saveTripDistance(float value){
        editor.putFloat("tripdistance",value);
        editor.commit();
    }

    public float getSaveTripTime(){
        return preferences.getFloat("triptime",0);
    }

    public float getSaveTripDistance(){
        return preferences.getFloat("tripdistance",0);
    }

    public void setAuthToken(String authtoken){
        editor.putString("auth_token",authtoken);
        editor.commit();
    }
    public String getAuthToken() {

        return preferences.getString("auth_token","");
    }

    public void setFirebaseToken(String token){
        editor.putString("firebase_token",token);
        editor.commit();
    }
    public String getFirebaseToken() {
        return preferences.getString("firebase_token","");
    }

    public void setFirebaseTokenUpdated(boolean updated){
        editor.putBoolean("firebase_token_update",updated);
        editor.commit();
    }

    public boolean isFirebaseTokenUpdated() {
        return preferences.getBoolean("firebase_token_update",false);
    }
}
